## no-recurse

no-recurse is a library for writing recursive algorithms without using recursion.

Recursion is great, but calling functions is slow because of the stack frame
being allocated and eventually you'll get a stack overflow.

no-recurse isn't a lot better at all, but the stack frame is allocated on the
heap which means it won't get a stack overflow as quickly and rust's vectors
take care of doubling the vector's size instead of allocating each item
separately, which should be slightly faster for lots of recursions.

If you can use a non-recursive algorithm or use tail-call recursion that's
probably still better than this. A tail-call function is still provided by this
library for the sake of it. It's implemented using a very simple loop and is
therefore as cheap as it gets.

This is currently just an experiment and syntax is likely to change... a lot.

## Usage

The library itself is written as generic functions that can be used, but on top
is a macro as an attempt to make the syntax less ugly.

The function `tail_call` takes an initial state and a callback that can choose
to either keep going or return, and returns whatever the callback chose to return.

The function `recurse` is a lot more complicated. It also takes an initial
state, but then it takes two different callbacks: Pre and post recursion. The
pre callback is called directly, and just like in `tail_call` whatever it
returns is ran. The difference here is that it can return any number of new
states, which will all be recursed in their respective orders. It can also
return any variable or structure for later: you'll see why in a second. When
all the recursed functions of one level have returned a non-continuing return
value, the post callback is called with the custom variable and a vector of
outputs from all functions. The post callback can't recurse any more - it has
to return a value that will be lifted back to the previous layer.

## Macro

The macro `no_recurse!` tries to hide the boilerplate and revert your code to a
nice and logical set of instructions.
First comes a function-like declaration syntax:
```
fn(nested: usize) -> u8
```
As you can see the return value is not optional and you'll need to use `()`.

Then comes the pre callback in a new block:
```rust
{
    if nested >= 3 {
        return Value::Return(0);
    }
    println!("nested {}: before recursing...", nested);
}
```

Then optionally come a series of variable declarations. This is because
variables from inside the block sadly aren't visible outside of it.
```
with next = nested + 1;
```

Then, also optionally, come a series of bound recursive calls:
```rust
let some_var = call (next);
let some_other_var = call (next);
```

Then comes the exact same thing as above, but for unbound recursive calls that
just ignore the return value.
```rust
call (next);
```

Now you can optionally pass a bunch of comma-separated variables
to forward from the first block to the second. Like previously stated, things
from inside the actual block can't sadly be accessed, but you can access the
function arguments and any `with` declarations.
```rust
forward nested, next
```

Almost there! Finally, the post callback:
```rust
{
    println!("nested {}: finished recursing {}", nested, next);
    1
}
```

Finally, the initial state
```rust
init (0)
```

The entire example you just read can be found in `examples/readme.rs`.
