use no_recurse::{no_recurse, Value};

fn main() {
    let res = fac(10);
    println!("{:?}", res);
}
fn fac(n: usize) -> usize {
    no_recurse! {
        fn(n: usize) -> usize {
            if n == 0 {
                return Value::Return(1);
            }
        }
        let result = call (n-1);
        forward n
        {
            n * result
        }
        init (n)
    }
}
