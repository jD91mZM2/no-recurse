use no_recurse::*;

fn main() {
    let res = fac(10);
    println!("{:?}", res);
}
fn fac(n: usize) -> usize {
    tail_call((n, 1), |(n, acc)| {
        if n == 0 {
            Value::Return(acc)
        } else {
            Value::Continue((n - 1, n * acc))
        }
    })
}
