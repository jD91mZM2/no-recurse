use smallvec::SmallVec;
use no_recurse::*;

fn main() {
    let res = fac(10);
    println!("{:?}", res);
}
fn fac(n: usize) -> usize {
    recurse(n, |n| {
        if n == 0 {
            Value::Return(1)
        } else {
            Value::Continue((n, [n-1]))
        }
    }, |n, res: SmallVec<[usize; 1]>| {
        n * res[0]
    })
}
