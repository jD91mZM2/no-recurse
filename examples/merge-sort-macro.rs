use no_recurse::{no_recurse, Value};
use rand::Rng;

fn main() {
    let mut rng = rand::thread_rng();
    for _ in 0..1000 {
        let mut input: [usize; 8] = [0; 8];
        for i in 0..input.len() {
            input[i] = rng.gen_range(0, 5);
        }
        let mut output = input;
        merge_sort(&mut output);
        for slice in output.windows(2) {
            if slice.len() == 2 {
                assert!(slice[0] <= slice[1], "sorting returned incorrect results: {:?}", input);
            }
        }
    }
}
fn merge_sort(data: &mut [usize]) {
    let mut buf = Vec::with_capacity(data.len() / 2);
    no_recurse! {
        fn(start: usize, end: usize) -> () {
            if start + 1 >= end {
                return Value::Return(());
            }
        }
        with mid = start + (end - start) / 2;
        call (start, mid);
        call (mid, end);
        forward start, mid, end
        {
            buf.resize(mid - start, 0);
            buf.copy_from_slice(&data[start..mid]);

            let mut first_i = 0;
            let mut second_i = mid;
            for i in start..end {
                data[i] = if first_i < buf.len() && (second_i >= end || buf[first_i] < data[second_i]) {
                    let val = buf[first_i];
                    first_i += 1;
                    val
                } else {
                    let val = data[second_i];
                    second_i += 1;
                    val
                };
            }
        }
        init (0, data.len())
    };
}
