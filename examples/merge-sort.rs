use smallvec::SmallVec;
use no_recurse::*;

fn main() {
    let mut data = [5, 9, 8, 3, 6, 1, 4, 2, 7];
    merge_sort(&mut data);
    println!("{:?}", data);
}
fn merge_sort(data: &mut [usize]) {
    let mut buf = Vec::with_capacity(data.len() / 2);
    recurse((0, data.len()), |(start, end)| {
        if start + 1 >= end {
            return Value::Return(());
        }
        let mid = start + (end - start) / 2;
        Value::Continue(((start, mid, end), [
            (start, mid),
            (mid, end)
        ]))
    }, |(start, mid, end), _: SmallVec<[(); 2]>| {
        buf.resize(mid - start, 0);
        buf.copy_from_slice(&data[start..mid]);

        let mut first_i = 0;
        let mut second_i = mid;
        for i in start..end {
            data[i] = if first_i < buf.len() && (second_i >= end || buf[first_i] < data[second_i]) {
                let val = buf[first_i];
                first_i += 1;
                val
            } else {
                let val = data[second_i];
                second_i += 1;
                val
            };
        }
    });
}
