use no_recurse::{no_recurse, Value};
use rand::Rng;

fn main() {
    let mut rng = rand::thread_rng();
    for _ in 0..1000 {
        let mut input: [usize; 8] = [0; 8];
        for i in 0..input.len() {
            input[i] = rng.gen_range(0, 5);
        }
        let mut output = input;
        quicksort(&mut output);
        for slice in output.windows(2) {
            if slice.len() == 2 {
                assert!(slice[0] <= slice[1], "sorting returned incorrect results: {:?}", input);
            }
        }
    }
}
fn quicksort<T: Ord + Clone>(data: &mut [T]) {
    no_recurse! {
        fn(start: usize, end: usize) -> ()
        {
            if start >= end {
                return Value::Return(());
            }
        }
        with pivot = {
            // Using middle because of https://en.wikipedia.org/wiki/Quicksort#Choice_of_pivot
            let pivot = data[(start + end) / 2].clone();
            let mut i = start;
            let mut j = end;
            // Hoare partition scheme:
            // Go from each end and swap the first two out of place elements:
            // For example, pretending 5 here is the piviot (though it won't be)
            // [3, 6, (5), 2, 7, 4]
            //     ^             ^
            // [3, 4, (5), 2, 7, 6]
            //         ^   ^
            // [3, 4, 2, (5), 7, 6]
            loop {
                while data[i] < pivot {
                    i += 1;
                }
                while data[j] > pivot {
                    j -= 1;
                }
                if i >= j {
                    break;
                }
                data.swap(i, j);
                i += 1;
                j -= 1;
            }
            j
        };
        call (start, pivot);
        call (pivot+1, end);
        // Macro sadly requies `forward` even if no variables are being forwarded
        forward
        {}
        init (0, data.len() - 1)
    }
}
