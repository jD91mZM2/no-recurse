#![allow(unused_variables)]
use no_recurse::{no_recurse, Value};

fn main() {
    no_recurse! {
        fn(nested: usize) -> u8 {
            if nested >= 3 {
                return Value::Return(0);
            }
            println!("{}- nested {}: before recursing...", " ".repeat(nested), nested);
        }
        with next = nested + 1;
        let some_var = call (next);
        let some_other_var = call (next);
        call (next);
        forward nested, next
        {
            println!("{}- nested {}: finished recursing {}", " ".repeat(nested), nested, next);
            1
        }
        init (0)
    };
}
