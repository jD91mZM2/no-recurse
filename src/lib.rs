use smallvec::SmallVec;

use std::mem;

mod macros;

pub enum Value<State, Final> {
    Continue(State),
    Return(Final)
}

/// Imitates a tail-call recursive function. This is implemented using a loop
/// and is incredibly cheap.
pub fn tail_call<State, Final, F>(initial: State, mut f: F) -> Final
    where F: FnMut(State) -> Value<State, Final>
{
    let mut value = initial;
    loop {
        match f(value) {
            Value::Continue(new) => value = new,
            Value::Return(new) => break new
        }
    }
}

#[derive(Clone, Copy)]
struct StackFrameId(usize);
struct StackFrame<State, Vars> {
    parent: Option<StackFrameId>,
    state: StackFrameState<State, Vars>
}
enum StackFrameState<State, Vars> {
    Initial(State),
    DonePre(Vars, Option<ValueId>),
    Placeholder
}
#[derive(Clone, Copy)]
struct ValueId(usize);
struct ValueItem<Val> {
    data: Val,
    prev_sibling: Option<ValueId>
}

/// A function that imitates recursion by calling specified callbacks pre and
/// post recursing.
pub fn recurse<State, Vars, Final, FnPre, FnPost, ArrayState, ArrayFinal>(
    initial: State,
    mut pre: FnPre,
    mut post: FnPost
) -> Final
    where
        ArrayState: smallvec::Array<Item = State>,
        ArrayFinal: smallvec::Array<Item = Final>,
        FnPre: FnMut(State) -> Value<(Vars, ArrayState), Final>,
        FnPost: FnMut(Vars, SmallVec<ArrayFinal>) -> Final
{
    let mut values: Vec<Option<ValueItem<Final>>> = Vec::new();
    let mut stack: Vec<StackFrame<State, Vars>> = Vec::new();
    stack.push(StackFrame {
        state: StackFrameState::Initial(initial),
        parent: None
    });
    loop {
        let id = stack.len() - 1;
        let item = &mut stack[id];
        let parent = item.parent;

        let mut push_value = None;
        match item.state {
            StackFrameState::Placeholder => unreachable!(),
            StackFrameState::Initial(_) => {
                let state = match mem::replace(&mut item.state, StackFrameState::Placeholder) {
                    StackFrameState::Initial(state) => state,
                    _ => unreachable!()
                };
                match pre(state) {
                    Value::Continue((vars, states)) => {
                        item.state = StackFrameState::DonePre(vars, None);
                        for child_state in SmallVec::from(states).into_iter().rev() {
                            stack.push(StackFrame {
                                state: StackFrameState::Initial(child_state),
                                parent: Some(StackFrameId(id))
                            });
                        }
                    },
                    Value::Return(value) => {
                        stack.pop().unwrap();

                        push_value = Some(ValueId(values.len()));
                        values.push(Some(ValueItem {
                            data: value,
                            prev_sibling: None
                        }));
                    }
                }
            },
            StackFrameState::DonePre(..) => {
                let (vars, value) = match stack.pop().unwrap().state {
                    StackFrameState::DonePre(vars, value) => (vars, value),
                    _ => unreachable!()
                };
                let mut vec = SmallVec::new();
                let mut next = value;
                while let Some(id) = next {
                    let value = values[id.0].take().unwrap();
                    next = value.prev_sibling;
                    vec.push(value.data);
                }

                let value = post(vars, vec.into());
                push_value = Some(ValueId(values.len()));
                values.push(Some(ValueItem {
                    data: value,
                    prev_sibling: None
                }));
            }
        };

        if let Some(push_id) = push_value {
            match parent {
                None => {
                    return values[push_id.0].take().unwrap().data;
                },
                Some(parent) => {
                    let value = match stack[parent.0].state {
                        StackFrameState::DonePre(_, ref mut value) => value,
                        _ => unreachable!()
                    };
                    values[push_id.0].as_mut().unwrap().prev_sibling = *value;
                    *value = Some(push_id);
                }
            }
        }
    }
}
