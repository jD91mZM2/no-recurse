#[macro_export]
macro_rules! no_recurse {
    (
        fn($($arg:ident: $type:ty),*) -> $ret:ty
            $pre:block
            $(with $scopevar:ident = $scopevalue:expr;)*
            $(let $callbind:ident = call ($($callarg:expr),*);)*
            $(call ($($callargextra:expr),*);)*
            forward $($forward:ident),*
            $post:block
        init ($($init:expr),*)
    ) => {
        $crate::recurse(($($init),*), |($($arg),*): ($($type),*)| {
            $pre;
            $(let $scopevar = $scopevalue;)*
            $crate::Value::Continue((($($forward),*), [$(($($callarg),*),)* $(($($callargextra),*)),*]))
        }, |vars, returns: smallvec::SmallVec<[$ret; 2]>| {
            let ($($forward),*) = vars;
            let mut iter = returns.into_iter();
            $(let $callbind = iter.next().unwrap();)*
            $post
        })
    };
}
